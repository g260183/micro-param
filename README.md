# micro-param

Repository containing the trained ML model and other scripts for replacing the two moment bulk microphysics scheme in ICON. 

## Creating the environment

Simply use the file `environment.yaml` for that purpose

## Fortran-python-communication

The file to change here is `warm-rain-nn.py`. It has to receive moments from Fortran and at the end, send out the desired output as well. Needs to be properly adapted.
It also has the mean and standard deviation arrays. 

## Solvers and trained models

The model class is stored in `models`. There are two classes: one the PyTorch model and the other is the Lightning Model that callas the PyTorch model.

The Solver class in `solvers/moment_solver.py` is an extra step. It called the model for predictions but is also needed for tasks such as normalization/re-normalization of inputs/outputs and creating the right input matrix for the trained model.

They are both intalized and called within the main script at `warm-rain-nn.py`.
